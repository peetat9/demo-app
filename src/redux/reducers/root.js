import * as types from '../actions/types'

const INITIAL_STATE = {
  user: 'testUSER',
  pass: 'testPASS',
}

export default  (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.CHG_USER:
      return {
        ...state,
        user: action.payload
      }
    case types.CHG_PASS:
      return {
        ...state,
        pass: action.payload
      }
    default:
      return state
  }
}