import * as types from './types'

export const chgRootUser = val => dispatch => dispatch({ type: types.CHG_USER, payload: val })
export const chgRootPass = val => dispatch => dispatch({ type: types.CHG_PASS, payload: val })
