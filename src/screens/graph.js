import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Dimensions,
} from 'react-native';
import { YAxis, XAxis, AreaChart, Grid, StackedAreaChart, LineChart } from 'react-native-svg-charts'
import { Circle, Path, G, Line, Rect, Text, Svg } from 'react-native-svg'
import _ from 'lodash'
import * as shape from 'd3-shape'

const widthX = Dimensions.get('window').width

const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ]
const dataX = [ 'S', 'M', 'T', 'W', 'T', 'F', 'S']

const Decorator = ({ x, y, data }) => {
  return data.map((value, index) => (
    <Circle
      key={ index }
      cx={ x(index) }
      cy={ y(value) }
      r={ 4 }
      stroke={ '#fff' }
      fill={ '#4ecd00' }
    />
  ))
}

const HorizontalLine = (({ stateX }) => (
  <Line
    key={ 'zero-axis' }
    x1={ `${(stateX/widthX) * 100}%` }
    x2={ `${(stateX/widthX) * 100}%` }
    y1={ '100%' }
    y2={ '-100%' }
    stroke={ 'grey' }
    strokeDasharray={ [ 4, 8 ] }
    strokeWidth={ 2 }
  />
))

const Shadow = ({ line }) => (
  <Path
    key={'shadow'}
    y={2}
    d={line}
    fill={'none'}
    strokeWidth={4}
    stroke={'rgba(0, 0, 0, 0.11)'}
  />
)

const Tooltip = ({ x, y }) => (
  <G
    x={ x(5) - (75 / 2) }
    key={ 'tooltip' }
    onPress={ () => console.log('tooltip clicked') }
  >
    <G y={ 50 }>
      <Rect
        height={ 40 }
        width={ 75 }
        stroke={ 'grey' }
        fill={ 'white' }
        ry={ 10 }
        rx={ 10 }
      />
        <Text
          x={ 75 / 2 }
          dy={ 20 }
          alignmentBaseline={ 'middle' }
          textAnchor={ 'middle' }
          stroke={ '#fff' }
        >
          { `${data[5]}ºC` }
        </Text>
      </G>
      <G x={ 75 / 2 }>
        <Line
          y1={ 50 + 40 }
          y2={ y(data[ 5 ]) }
          stroke={ 'grey' }
          strokeWidth={ 2 }
        />
        <Circle
          cy={ y(data[ 5 ]) }
          r={ 6 }
          stroke={ '#fff' }
          strokeWidth={ 2 }
          fill={ 'white' }
        />
    </G>
  </G>
)

export default class App extends React.Component {
  state = {
    x: 0,
    touchStatus: false,
  }

  handleX = _.debounce((x) => this.setState({ x }), 10)
  onToggleStatusTounch = _.debounce((val) => this.setState({ touchStatus: val }), 10)

  render() {
    const contentInset = { top: 20, bottom: 20 }
    console.log('state: ', this.state)
    return (
      <View style={{ justifyContent: 'center' }}>
        <View style={{ height: 200, width: '100%', backgroundColor: this.state.touchStatus ? 'green':'red' }}/>
        <View style={{ backgroundColor: '#4ecd00'}}>
          <View style={{ height: 200, flexDirection: 'row' }} 
            onTouchMove={(e)=> {
              console.log('onTouchMove')
              this.onToggleStatusTounch(true)
              this.handleX(e.nativeEvent.locationX)
            }}
            onTouchEnd={() => {
              console.log('onTouchEnd')
              this.onToggleStatusTounch(false)
            }}
          >
            <LineChart
              style={{ flex: 1 }}
              data={ data }
              svg={{
                stroke: '#fff',
                strokeWidth: 2,
              }}
              // curve={ shape.curveNatural }
              contentInset={ contentInset }
            >
              <Grid
                svg={{
                  opacity: 0.7,
                  stroke: '#fff',
                  strokeWidth: 0.2,
                }}
              />
              <Shadow/>
              <HorizontalLine stateX={this.state.x}/>
              <Decorator/>
              {/* <Tooltip/> */}
            </LineChart>
            <YAxis
              style={{ position: 'absolute', top: 0, bottom: 0 }}
              data={ data }
              contentInset={ contentInset }
              svg={{
                fontFamily: "Kanit",
                fontSize: 12,
                opacity: 0.7,
                fill: 'white',
                strokeWidth: 0.1,
                alignmentBaseline: 'baseline',
                baselineShift: '3',
              }}
              numberOfTicks={ 10 }
              formatLabel={ value => value }
            />
          </View>
          <XAxis
            // style={{ marginHorizontal: -10 }}
            data={ data }
            formatLabel={ (value, index) => dataX[index] }
            contentInset={ contentInset }
            svg={{ fontSize: 10, fill: 'black' }}
          />
        </View>
      </View>
    );
  }
}
