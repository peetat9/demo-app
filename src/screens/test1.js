import React from 'react'
import { StyleSheet, Text, View, TextInput} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../redux/actions'

const mapStateToProps = (state) => ({ state })
const mapDispatchToProps = (dispatch) => ({
  chgRootUser: bindActionCreators(actions.chgRootUser, dispatch),
  chgRootPass: bindActionCreators(actions.chgRootPass, dispatch),
})
class A extends React.Component {
  render = () => {
    const { root } = this.props.state
    console.log('User: ', root.user)
    console.log('Pass: ', root.pass)
    return (
      <View style={{ backgroundColor: 'red', width: '100%', height: '100%' }}>
        <Text>Screen 1</Text>
        <Text>User</Text><TextInput value={root.user} onChangeText={this.props.chgRootUser} />
        <Text>Pass</Text><TextInput value={root.pass} onChangeText={this.props.chgRootPass} />
      </View>
    )
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(A)