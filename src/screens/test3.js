import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
const TestComp = {
  a : () => (
    <View style={{ backgroundColor: 'red' }}>
      <Text>TESSSSST</Text>
    </View>
  )
}
export default class extends React.Component {
  render = () => {
    return (
      <View style={{ backgroundColor: 'blue', width: '100%', height: '100%' }}>
        <Text>Screen 3</Text>
        <TestComp.a/>
      </View>
    )
  }
}