import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TextInput, 
  Alert, 
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native'
import {
  CameraKitCameraScreen,
  CameraKitCamera,
} from 'react-native-camera-kit'

const {
  height,
  width,
} = Dimensions.get('window')

class AAA extends React.Component {
  state = {
    cap: true,
    img: '',
  }
  componentDidMount = () => {
    // this.props.navigator.setStyle({ navBarHidden: true })
  }

  onBottomButtonPressed(event) {
    const captureImages = JSON.stringify(event.captureImages);
    Alert.alert(
      `${event.type} button pressed`,
      `${captureImages}`,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    )
  }

  cap = async () => {
    const image = await this.camera.capture(false)
    this.setState({
      cap: false,
      img: image.uri,
    })
    console.log('image: ', image)
  }

  render = () => {
    return (
      <View>
      {
        this.state.cap ? 
        <View>
          <CameraKitCamera
            style={{
              width: '100%', 
              height: '100%',
              backgroundColor: '#000',
              position: 'relative',

            }}
            ref={cam => this.camera = cam}        
          />
          <Image
            style={{
              position: 'absolute',
              top: '5%',
              left: width/2 - 141,
              justifyContent: 'center',
            }}
            source={require('../../assets/mark.png')}
          />
          <TouchableOpacity
            onPress={()=>{this.cap()}}
            style={{
              position: 'absolute',
              bottom: 20,
              left: width/2 - 35,
            }}
          >
            <Image
              style={{
                width: 70, 
                height: 70,
              }}
              source={require('../../assets/images/cameraButton.png')}
            />
          </TouchableOpacity>
        </View>
        :
        <View>
          <Image style={{ width: '100%', height: '100%' }} source={{ uri: this.state.img }} />
        </View>
      }
      </View>
      // <CameraKitCameraScreen
      //   actions={{ rightButtonText: 'Done', leftButtonText: 'Cancel' }}
      //   onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
      //   flashImages={{
      //     on: require('../../assets/images/flashOn.png'),
      //     off: require('../../assets/images/flashOff.png'),
      //     auto: require('../../assets/images/flashAuto.png')
      //   }}
      //   cameraFlipImage={require('../../assets/images/cameraFlipIcon.png')}
      //   captureButtonImage={require('../../assets/images/cameraButton.png')}
      // />
    )
  }
}

export default AAA