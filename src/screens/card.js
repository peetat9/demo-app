import React from 'react'
import { StyleSheet, Text, View, TextInput} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../redux/actions'
import Card from '../components/ParallexSwiper'
import data from '../untility/data'

const mapStateToProps = (state) => ({ state })
const mapDispatchToProps = (dispatch) => ({
  chgRootUser: bindActionCreators(actions.chgRootUser, dispatch),
  chgRootPass: bindActionCreators(actions.chgRootPass, dispatch),
})



class A extends React.Component {
  state = {
    navStatus: false
  }
  componentDidMount = () => {
    this.props.navigator.setStyle({ navBarHidden: this.state.navStatus })
  }
  distableNav = () => {
    console.log('distableNav')
    this.setState(({navStatus}) => {
      console.log('navStatus: ', navStatus)
      return {navStatus: !navStatus}
    }, () => {
      console.log('call back navStatus: ', this.state.navStatus)
      this.props.navigator.setStyle({ navBarHidden: this.state.navStatus })
    })
  }
  render = () => {
    const { root } = this.props.state
    console.log('state: ', this.state.navStatus)
    console.log('User: ', root.user)
    console.log('Pass: ', root.pass)
    return (
      <Card data={data} distableNav={this.distableNav}/>
    )
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(A)