import React from 'react'
import { connect } from 'react-redux'
import { StyleSheet, Text, View } from 'react-native'

const mapStateToProps = (state) => ({ state })
class B extends React.Component {
  render = () => {
    const { root } = this.props.state
    return (
      <View style={{ backgroundColor: 'green', width: '100%', height: '100%' }}>
        <Text>Screen 2</Text>
        <Text>User: {root.user}</Text>
        <Text>Pass: {root.pass}</Text>
      </View>
    )
  }
}

export default connect(mapStateToProps)(B)
