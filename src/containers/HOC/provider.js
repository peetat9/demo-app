import { Provider } from 'react-redux';
import React from 'react';
import hoistNonReactStatic from 'hoist-non-react-statics'
import store from '../../redux/store'

export default (WrappedComponent) => {
  const Enhance = props => (
    <Provider store={store}>
      <WrappedComponent {...props} />
    </Provider>
  )
  hoistNonReactStatic(Enhance, WrappedComponent)
  return Enhance
}