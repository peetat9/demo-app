import { Navigation, ScreenVisibilityListener } from 'react-native-navigation'
import provider from '../containers/HOC/provider'
import test1 from '../screens/test1'
import test2 from '../screens/test2'
import test3 from '../screens/test3'
import graph from '../screens/graph'
import card from '../screens/card'
import camera from '../screens/camera'

export const registerScreens = () => {
  Navigation.registerComponent('example.test1', () => provider(test1))
  Navigation.registerComponent('example.test2', () => provider(test2))
  Navigation.registerComponent('example.test3', () => provider(test3))
  Navigation.registerComponent('example.graph', () => provider(graph))
  Navigation.registerComponent('example.card', () => provider(card))
  Navigation.registerComponent('example.camera', () => provider(camera))
}

export const registerScreenVisibilityListener = () => {
  new ScreenVisibilityListener({
    willAppear: ({ screen }) => console.log(`Displaying screen ${screen}`),
    didAppear: ({
      screen, startTime, endTime, commandType,
    }) => console.log('screenVisibility', `Screen ${screen} displayed in ${endTime - startTime} millis [${commandType}]`),
    willDisappear: ({ screen }) => console.log(`Screen will disappear ${screen}`),
    didDisappear: ({ screen }) => console.log(`Screen disappeared ${screen}`),
  }).register()
}
