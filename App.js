import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Platform,
} from 'react-native'
import { Navigation } from 'react-native-navigation'
import { registerScreens, registerScreenVisibilityListener } from './src/config/routers'


// export default class App extends React.Component {
//   render = () => {
//     return (
//       <Provider store={store}>
//         <View style={{ justifyContent: 'center', backgroundColor: 'blue' }}>
//           <Text>TEST</Text>
//         </View>
//       </Provider>
//     );
//   };
// };

registerScreens()
registerScreenVisibilityListener()

// const tabs = [
//   {
//     label: 'camerA',
//     screen: 'example.camera',
//     icon: require('./assets/img/test/img/swap.png'),
//     title: 'camera test',
//   }, {
//     label: 'Card',
//     screen: 'example.card',
//     icon: require('./assets/img/test/img/swap.png'),
//     title: 'Card Test',
//   }, {
//     label: 'Graph',
//     screen: 'example.graph',
//     icon: require('./assets/img/test/img/swap.png'),
//     title: 'Graph Test',
//   }, {
//     label: 'Navigation',
//     screen: 'example.test1',
//     icon: require('./assets/img/test/img/list.png'),
//     title: 'Redux In',
//   }, {
//     label: 'Actions',
//     screen: 'example.test2',
//     icon: require('./assets/img/test/img/swap.png'),
//     title: 'Redux Out',
//   },
// ]

// Navigation.startTabBasedApp({
//   tabs,
//   // drawer: {
//   //   left: {
//   //     screen: 'example.test3',
//   //   },
//   // },
// })

Navigation.startSingleScreenApp({
  screen: {
    screen: 'example.camera',
    title: 'ถ่ายบัตรประชาชน',
  },
})

// const A = () => (
//   <View style={{ backgroundColor: 'red' }}>
//     <Text>Open up App.js to start working on your app1</Text>
//   </View>
// )

// const B = () => (
//   <View style={{ backgroundColor: 'greed' }}>
//     <Text>Open up App.js to start working on your app2</Text>
//   </View>
// )

// const C = () => (
//   <View style={{ backgroundColor: 'blue' }}>
//     <Text>Open up App.js to start working on your app3</Text>
//   </View>
// )
// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Open up App.js to start working on your app!</Text>
//         <Text>Changes you make will automatically reload.</Text>
//         <Text>Shake your phone to open the developer menu.</Text>
//         <A/>
//         <B/>
//         <C/>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
